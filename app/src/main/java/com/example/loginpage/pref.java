package com.example.loginpage;

import android.content.Context;
import android.content.SharedPreferences;

public class pref {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public String islogin="islogin";
    public pref(Context context)
    {
        preferences=context.getSharedPreferences(context.getPackageName(),context.MODE_PRIVATE);
        editor=preferences.edit();
    }

    public void setIslogin(Boolean status)
    {
        editor.putBoolean(islogin,status);
        editor.commit();
        editor.apply();
    }

    public boolean getIsLogin()
    {
        return preferences.getBoolean(islogin,false);
    }
}
