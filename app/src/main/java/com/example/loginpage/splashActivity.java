package com.example.loginpage;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class splashActivity extends AppCompatActivity {
    pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        pref=new pref(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(pref.getIsLogin())
                startActivity(new Intent(splashActivity.this,Home_Activity.class));
                else
                    startActivity(new Intent(splashActivity.this,MainActivity.class));

            }
        },5000);
    }
}
