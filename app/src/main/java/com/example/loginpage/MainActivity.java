package com.example.loginpage;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText Name;
    private EditText Password;
    private Button Login, Signup;
    private int counter=5;
    pref pref;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Name = findViewById(R.id.etUserName);
        Password = findViewById(R.id.etPassword);
        Login = findViewById(R.id.btnLogin);
        Signup = findViewById(R.id.btnSignup);
        Login.setOnClickListener(this);
        Signup.setOnClickListener(this);
        pref=new pref(this);

    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id)
        {
            case R.id.btnLogin:
                pref.setIslogin(true);
                startActivity(new Intent(this,Home_Activity.class));
                break;
            case R.id.btnSignup:
                signup();
                break;
        }
    }

    private void dialog() {
        AlertDialog dialog;
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("For Guest Account");
        builder.setMessage("USer= Sourav && Password=12345");
        dialog=builder.create();
        dialog.show();
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this,"Exit",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
           }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this,"OK",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void signup() {
        Intent in=new Intent(MainActivity.this,Main2Activity.class);
        startActivity(in);

    }

    private void login() {
        String username=Name.getText().toString().trim();
        String password=Password.getText().toString().trim();
        if(username.isEmpty()||password.isEmpty())
        {
            dialog();
            util.toast(this,"All fields Are required");
        }
        else {
        dologin(username,password);
        }
    }

    private void dologin(String username,String password) {

        Intent intent=new Intent(MainActivity.this,Home_Activity.class);
        startActivity(intent);

    util.toast(this,username+" "+password);

    }


}

